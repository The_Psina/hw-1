TARGET = main.out
TEST_TARGET = tests.out
HDRS_DIR = \
	include

SRCS = \
	src/main.c \
	src/to_lower_and_sort.c

.PHONY: all clean main

all: main

main: $(SRCS)
	$(CC) -Wall -Wextra -Werror -I  $(HDRS_DIR) -o $(TARGET) $(CFLAGS) $(SRCS)

clean:
	rm -rf $(TARGET)
