#ifndef INCLUDE_TO_LOWER_AND_SORT_H_
#define INCLUDE_TO_LOWER_AND_SORT_H_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<malloc.h>

#define LOWER_LEFT_LETTER 97  // a
#define LOWER_RIGHT_LETTER 122  // z

#define UPPER_LEFT_LETTER 65  // A
#define UPPER_RIGHT_LETTER 90  // Z

#define DIFFERRENCE 32


char* get_string();
size_t count_words(char*);
void custom_free(char**);
void parse(char**,const char*);
size_t custom_to_lower(char**);
size_t custom_sort(char**);
size_t custom_print(char**);

#endif  // INCLUDE_TO_LOWER_AND_SORT_H_
