#include "to_lower_and_sort.h"

int main() {
    char* input_str = get_string();  // input and inicialize string
    if (input_str == NULL) {
        printf("%s", strerror(EFAULT));
        return EFAULT;
    }

    size_t word_count = count_words(input_str);
    char** array = (char**)malloc(word_count * sizeof(char*));
    parse(array, input_str);
    printf("\nParsed array:\n");
    if (custom_print(array)) {
        free(input_str);
        printf("here %s", strerror(EFAULT));
        return EFAULT;
    }

    printf("\nLower array:\n");
    if (custom_to_lower(array) || custom_print(array)) {
        free(input_str);
        printf("%s", strerror(EFAULT));
        return EFAULT;
    }
    
    printf("\nSorted array:\n");
    if (custom_sort(array) || custom_print(array)) {
        free(input_str);
        printf("%s", strerror(EFAULT));
        return EFAULT;
    }

    free(input_str);
    custom_free(array);

    return 0;
}

