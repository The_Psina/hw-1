#include "to_lower_and_sort.h"

static size_t check(char);  // check symbol in ASCII table
static size_t separator_check(char);

char* get_string() {
    char* string = NULL;
    size_t count = 0;
    char c = '\0';
    while (scanf("%c", &c), c != EOF && c != '\n') {
        count++;

        char* tmp = (char*)malloc((count + 1) * sizeof(char));
        if (tmp == NULL) {
            if (string) {
                free(string);
            }
            return NULL;
        }
        if (string) {
            memmove(tmp, string, count);
            free(string);
        }

        string = tmp;

        string[count - 1] = c;
        string[count] = '\0';
    }

    return string;
}

size_t count_words(char *str) {
    size_t i = 0;
    size_t words = 0;
    while (str[i] != '\0') {
        if (!separator_check(str[i]) && separator_check(str[i + 1])) {
            words++;
        }
        i++;
    }
    return words + 1;
}

void parse(char** array, const char* str) {
    size_t array_iterator = 0;
    size_t i = 0;
    size_t word_len = 1;
    while (str[i] != '\0') {
        if (!separator_check(str[i]) && separator_check(str[i + 1])) {  // check last letter of word
            size_t left_symbol = i;
            while (left_symbol != 0) {  // counting word length
                if (separator_check(str[left_symbol])) {
                    word_len--;
                    break;
                } else {
                    word_len++;
                    left_symbol--;
                }
            }

            array[array_iterator] = (char*)malloc((word_len + 1) * sizeof(char));  // memory for word and '\0'
            if (!left_symbol) {
                memmove(array[array_iterator], str + left_symbol, word_len);
            } else {
                memmove(array[array_iterator], str + left_symbol + 1, word_len);
            }
            array[array_iterator][word_len] = '\0';

            array_iterator++;
            word_len = 1;
        }
        i++;
    }
    array[array_iterator] = NULL;
}

size_t custom_to_lower(char** array) {
    size_t i = 0;
    while (array[i] != NULL) {
        size_t word_len = 1;
        for (size_t j = 0; array[i][j] != '\0'; j++) {
            word_len++;
        }

        for (size_t j = 0; array[i][j] != '\0'; j++) {
            if(check(array[i][j])) {
                return 1;
            }
            if (array[i][j] >= UPPER_LEFT_LETTER && array[i][j] <= UPPER_RIGHT_LETTER) {
                array[i][j] = array[i][j] + DIFFERRENCE;
            }
        }
        i++;
    }
    array[i] = NULL;
    return 0;
}

size_t custom_sort(char** array) {
    if (array == NULL) {
        return 1;
    }
    size_t i = 0;
    while (array[i] != NULL) {
        for (size_t j = i + 1; array[j] != NULL; j++) {
            if (array[i][0] <= array[j][0]) {
                size_t word_len_i = 1;
                for (size_t k = 0; array[i][k] != '\0'; k++) {
                    word_len_i++;
                }
                size_t word_len_j = 1;
                for (size_t k = 0; array[j][k] != '\0'; k++) {
                    word_len_j++;
                }

                char* tmp = (char*)malloc(word_len_i * sizeof(char));
                if (tmp == NULL) {
                    return 1;
                }

                memmove(tmp, array[i], word_len_i);

                free(array[i]);
                array[i] = (char*)malloc(word_len_j * sizeof(char));
                if (array[i] == NULL) {
                    free(tmp);
                    return 1;
                }

                memmove(array[i], array[j], word_len_j);

                free(array[j]);
                array[j] = (char*)malloc(word_len_i * sizeof(char));
                if (array[j] == NULL) {
                    free(tmp);
                    return 1;
                }

                memmove(array[j], tmp, word_len_i);
                free(tmp);
            }
        }
        i++;
    }
    return 0;
}

size_t custom_print(char** array) {
    if (array == NULL) {
        return 1;
    }
    size_t i = 0;
    while (array[i] != NULL) {
        printf("%s\n", array[i]);
        i++;
    }
    return 0;
}

size_t check(char c) {
    if (c == ' ') {
        return 0;
    }
    if (c < UPPER_LEFT_LETTER) {
        return 1;
    }
    if (c > UPPER_RIGHT_LETTER && c < LOWER_LEFT_LETTER) {
        return 1;
    }
    if (c > LOWER_RIGHT_LETTER) {
        return 1;
    }
    return 0;
}

size_t separator_check(char c) {
    if (c == '\0') {
        return 1;
    }
    if (c == ' ') {
        return 1;
    }
    if (c == '.') {
        return 1;
    }
    if (c == ',') {
        return 1;
    }
    return 0;
}

void custom_free(char** array) {
    for (size_t i = 0; array[i] != NULL; i++) {
        free(array[i]);
    }
    free(array);
}

