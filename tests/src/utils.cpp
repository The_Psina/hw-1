#include "gtest/gtest.h"
#include<cstring>

extern "C" {
	#include "../include/to_lower_and_sort.h"
}

TEST(get_string, ok) {
	char* input_string = "hello dear friend";
	char* output_string = get_string(INPUT_FILE);
	ASSERT_STREQ(input_string, output_string);
	free(output_string);
}

TEST(parse, ok) {
    char* input_array[3] = {"hello", "dear", "friend"};

    char* input_string = get_string(INPUT_FILE);

    size_t word_count = count_words(input_string);
    char** output_array = (char**)malloc(word_count * sizeof(char*));;
    parse(output_array, input_string);

    for (size_t i = 0; i < 3; i++) {
        ASSERT_STREQ(input_array[i], output_array[i]);
    }

    custom_free(output_array);
    free(input_string);
}

TEST(lower, ok) {
    char* input_array[7] = {"hello", "dear", "friend", "it", "is", "lower", "test"};

    char* input_string = get_string(INPUT_FILE_LOWER);

    size_t word_count = count_words(input_string);
    char** output_array = (char**)malloc(word_count * sizeof(char*));;
    parse(output_array, input_string);

    custom_to_lower(output_array);

    for (size_t i = 0; i < 7; i++) {
        ASSERT_STREQ(input_array[i], output_array[i]);
    }

    custom_free(output_array);
    free(input_string);
}

TEST(sort, ok) {
    char* input_array[7] = {"test", "lower", "is", "it", "hello", "friend", "dear"};
    char* input_string = get_string(INPUT_FILE_LOWER);

    size_t word_count = count_words(input_string);
    char** output_array = (char**)malloc(word_count * sizeof(char*));;
    parse(output_array, input_string);

    custom_to_lower(output_array);

    custom_sort(output_array);

    for (size_t i = 0; i < 7; i++) {
        ASSERT_STREQ(input_array[i], output_array[i]);
    }

    custom_free(output_array);
    free(input_string);
}
